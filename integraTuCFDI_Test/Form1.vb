﻿Imports integraTuCFDI_Test.ws_integratucfdi
Public Class Form1
    Private Sub BtnTimbrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnTimbrarFactura.Click
        Dim objCFDI As New comprobante
        'Informacion general del comprobante
        objCFDI.serie = "A"
        objCFDI.folio = 1
        objCFDI.fecha = Now.ToString("yyyy-MM-dd HH:mm:ss") 'Fecha del comprobante, no debe de exeder de 72 hrs para poder ser timbrado(ejemplo del formato 2021-04-14 12:51:03)
        objCFDI.formapago = "01"
        objCFDI.condicionesDePago = "01"
        objCFDI.subtotal = 1210
        objCFDI.descuento = 10
        objCFDI.total = 1392
        objCFDI.moneda = "MXN"
        objCFDI.tipocambio = 1
        objCFDI.metodopago = "PUE"
        objCFDI.tipodocumento = "I"
        
        'CFDI Relacionados al comprobante
        objCFDI.cfdirelacionados = New CFDIRelacionados
        objCFDI.cfdirelacionados.TipoRelacion = "01"
        objCFDI.cfdirelacionados.UUIDS = "FA970432-279C-4336-A3BA-49C367EE76E2,76E20432-279C-4336-A3BA-49C367EEFA97" 'Separados por coma
        'Informacion del receptor del comprobante

        objCFDI.receptor = New receptor
        objCFDI.receptor.rfc = "HECJ771113E94"
        objCFDI.receptor.nombre = "JESUS DIEGO HERNANDEZ CANIZALEZ"
        objCFDI.receptor.usodelcomprobante = "G01"
        'Conceptos del comprobante
        Dim objconceptos(1) As concepto
        'Primer concepto
        objconceptos(0) = New concepto
        objconceptos(0).cantidad = 2
        objconceptos(0).clave = "20122807"
        objconceptos(0).codigo = "002"
        objconceptos(0).descripcion = "MANIOBRAS"
        objconceptos(0).codigounidad = "E48" 'Codigo de unidad SAT
        objconceptos(0).unidad = "SERVICIO"
        objconceptos(0).valorUnitario = 105
        objconceptos(0).importe = 210
        objconceptos(0).descuento = 10
        'Impuestos trasladados del concepto
        Dim traslados(1) As impuestotrasladadoconcepto
        traslados(0) = New impuestotrasladadoconcepto
        traslados(0).base = 200
        traslados(0).factor = "Tasa"
        traslados(0).impuesto = "002"
        traslados(0).tasaocuota = 0.16
        traslados(0).importe = 32
        traslados(1) = New impuestotrasladadoconcepto
        traslados(1).base = 200
        traslados(1).factor = "Tasa"
        traslados(1).impuesto = "003"
        traslados(1).tasaocuota = 0.5
        traslados(1).importe = 100
        objconceptos(0).impuestostrasladados = traslados
        'Segundo Concepto
        objconceptos(1) = New concepto
        objconceptos(1).cantidad = 1
        objconceptos(1).clave = "78121603"
        objconceptos(1).codigo = "001"
        objconceptos(1).descripcion = "FLETE DE ACARREO DE COMPOSTA"
        objconceptos(1).codigounidad = "H87"
        objconceptos(1).unidad = "PIEZA"
        objconceptos(1).valorUnitario = 1000
        objconceptos(1).importe = 1000
        'Impuestos trasladados del concepto
        ReDim traslados(0)
        traslados(0) = New impuestotrasladadoconcepto
        traslados(0).base = 1000
        traslados(0).factor = "Tasa"
        traslados(0).impuesto = "002"
        traslados(0).tasaocuota = 0.16
        traslados(0).importe = 160
        objconceptos(1).impuestostrasladados = traslados
        'Impuestos retenidos del concepto
        Dim retenciones(0) As impuestoretenidoconcepto
        retenciones(0) = New impuestoretenidoconcepto
        retenciones(0).base = 1000
        retenciones(0).factor = "Tasa"
        retenciones(0).impuesto = "001"
        retenciones(0).tasaocuota = 0.1
        retenciones(0).importe = 100
        objconceptos(1).impuestosretenidos = retenciones
        'Agregamos los conceptos al comprobante
        objCFDI.conceptos = objconceptos

        'Impuestos Trasladados del comprobante
        Dim objimptrasladados As New traslados
        objimptrasladados.totaltraslados = 292
        Dim impuestostrasladados(1) As impuestotrasladado
        impuestostrasladados(0) = New impuestotrasladado
        impuestostrasladados(0).impuesto = "002"
        impuestostrasladados(0).tipofactor = "Tasa"
        impuestostrasladados(0).tasaocuota = 0.16
        impuestostrasladados(0).importe = 192
        impuestostrasladados(1) = New impuestotrasladado
        impuestostrasladados(1).impuesto = "003"
        impuestostrasladados(1).tipofactor = "Tasa"
        impuestostrasladados(1).tasaocuota = 0.5
        impuestostrasladados(1).importe = 100
        objimptrasladados.impuestostrasladados = impuestostrasladados
        objCFDI.traslados = objimptrasladados
        'Impuestos Retenidos
        Dim objimpretenidos As New retenciones
        objimpretenidos.totalretenciones = 100
        Dim impuestosretenidos(0) As impuestoretenido
        impuestosretenidos(0) = New impuestoretenido
        impuestosretenidos(0).impuesto = "001"
        impuestosretenidos(0).importe = 100
        objimpretenidos.impuestosretenidos = impuestosretenidos
        objCFDI.retenciones = objimpretenidos
        Dim obj As New wscfdi
        Dim objws As RespuestaTimbrado = obj.GenerarComprobante(5090, "AAA010101AAA", 4171, objCFDI, "usrdemo", "8b60afbf328296b33204c65cdc149952")
        If objws.resultado Then
            'obtener el zip que contiene el xml y el CBB
            Dim vec() As Byte
            vec = Convert.FromBase64String(objws.archivos)
            Dim fs As New IO.FileStream("comprobante_factura.zip", IO.FileMode.Create, IO.FileAccess.Write)
            fs.Write(vec, 0, vec.Length)
            fs.Close()
            Dim strDatosTimbre As String = ""
            strDatosTimbre &= "uuid:" & objws.uuid & vbCrLf
            strDatosTimbre &= "fecha:" & objws.fechadocumento & vbCrLf
            strDatosTimbre &= "fechatimbre:" & objws.fechatimbre & vbCrLf
            strDatosTimbre &= "certificado emisor:" & objws.seriecertificado & vbCrLf
            strDatosTimbre &= "certificado sat:" & objws.seriecertificadosat & vbCrLf
            strDatosTimbre &= "sello sat:" & objws.sellotimbre & vbCrLf
            strDatosTimbre &= "cadena timbre:" & objws.cadenaoriginal & vbCrLf
            strDatosTimbre &= "sello digital:" & objws.sellodocumento & vbCrLf
            MessageBox.Show("Comprobante generado correctamente" & vbCrLf & strDatosTimbre)
        Else
            MessageBox.Show(objws.mensaje)
        End If
    End Sub

    Private Sub BtnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancelar.Click
        Dim obj As New wscfdi
        Dim objws As CancelarCfdiResponse = obj.CancelarCFDI(5090, "AAA010101AAA", "49530B40-7E57-4255-9CBB-DE28FB19F813", 4171, "usrdemo", "8b60afbf328296b33204c65cdc149952")
        If objws.resultado Then
            'En caso de obtener un resultado satisfactorio el mensaje contiene la fecha de cancelacion y el sello de acuce de cancelacion separados por un pipe
            MessageBox.Show(objws.mensaje)
        Else
            MessageBox.Show("No se logro cancelar el comprobante" & objws.mensaje)
        End If
    End Sub

    Private Sub BtnTimbrarPago_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnTimbrarPago.Click
        Dim objCFDI As New ReciboRecepcionPagos
        'Informacion general del comprobante
        objCFDI.serie = "P"
        objCFDI.folio = 1
        objCFDI.fecha = Now.ToString("yyyy-MM-dd HH:mm:ss") 'Fecha del comprobante, no debe de exeder de 72 hrs para poder ser timbrado(ejemplo del formato 2021-04-14 12:51:03)
        'objCFDI.uuidpagosustituye = "FA970432-279C-4336-A3BA-49C367EE76E2" 'en caso de ser un comprobante de pago que sustituye a otro comprobante previamente cancelado

        'Informacion del receptor del comprobante
        objCFDI.receptor = New receptor
        objCFDI.receptor.rfc = "HECJ771113E94"
        objCFDI.receptor.nombre = "JESUS DIEGO HERNANDEZ CANIZALEZ"
        'Ejemplo con dos pagos recibidos
        Dim Pagos(0 To 1) As pago
        Pagos(0) = New pago
        Pagos(0).fechapago = "2021-03-20T10:05:56"
        Pagos(0).formadepago = "01"
        Pagos(0).moneda = "MXN"
        Pagos(0).monto = 1245.56
        'Cfdis pagados en el primer pago
        Dim cfdipagados(0 To 0) As cfdi
        cfdipagados(0) = New cfdi
        cfdipagados(0).uuid = "DAC78D0E-7E57-4733-A1E2-223147A9B940"
        cfdipagados(0).serie = "A"
        cfdipagados(0).folio = 5
        cfdipagados(0).moneda = "MXN"
        cfdipagados(0).metodopago = "PPD"
        cfdipagados(0).numeroparcialidad = 1
        cfdipagados(0).importesaldoanterior = 1245.56
        cfdipagados(0).importepagado = 1245.56
        cfdipagados(0).importesaldoinsoluto = 0
        'Agregamos los cfdi pagados del primer pago
        Pagos(0).cfdis = cfdipagados

        'Llenamos el segundo pago
        Pagos(1) = New pago
        Pagos(1).fechapago = "2021-04-01T14:56:35"
        Pagos(1).formadepago = "02"
        Pagos(1).moneda = "USD"
        Pagos(1).tipocambio = 18.1621
        Pagos(1).monto = 181.621
        Pagos(1).numerooperacion = "123432"
        Pagos(1).rfcentidademisoracuentaorigen = "XEXX010101000" 'RFC del banco de donde se emite el pago
        Pagos(1).nombrebancoordenante = "BANK OF AMERICA" 'Nombre del banco de donde se emite el pago
        Pagos(1).rfcentidademisoracuentabeneficiario = "BMN930209927" 'RFC del Banco de la cuenta en donde se recibe el pago
        Pagos(1).cuentabeneficiario = "0208833370" 'Numero de cuenta en donde se recibe el pago
        'Segundo pago (ejemplo con abono a 2 facturas)
        ReDim cfdipagados(0 To 1)
        'Primer factura del segundo pago
        cfdipagados(0) = New cfdi
        cfdipagados(0).uuid = "458A8C7D-7E57-49A4-9F62-25519C47DBF3"
        cfdipagados(0).serie = "A"
        cfdipagados(0).folio = 7
        cfdipagados(0).moneda = "USD"
        cfdipagados(0).tipocambio = 18.1621
        cfdipagados(0).metodopago = "PPD"
        cfdipagados(0).numeroparcialidad = 1
        cfdipagados(0).importesaldoanterior = 90.8105
        cfdipagados(0).importepagado = 90.8105
        cfdipagados(0).importesaldoinsoluto = 0
        'Segunda factura del segundo pago
        cfdipagados(1) = New cfdi
        cfdipagados(1).uuid = "656BB828-7E57-4623-9B1C-ABA2816F2C06"
        cfdipagados(1).serie = "A"
        cfdipagados(1).folio = 8
        cfdipagados(1).moneda = "USD"
        cfdipagados(1).tipocambio = 18.1621
        cfdipagados(1).metodopago = "PPD"
        cfdipagados(1).numeroparcialidad = 1
        cfdipagados(1).importesaldoanterior = 90.8105
        cfdipagados(1).importepagado = 90.8105
        cfdipagados(1).importesaldoinsoluto = 0
        'Agregamos las facturas al segundo pago
        Pagos(1).cfdis = cfdipagados

        objCFDI.pagos = Pagos
        Dim obj As New wscfdi
        Dim objresult As RespuestaTimbrado = obj.GeneraComprobantePago(5090, "AAA010101AAA", 4171, objCFDI, "usrdemo", "8b60afbf328296b33204c65cdc149952")
        If objresult.resultado Then
            Dim vec() As Byte
            vec = Convert.FromBase64String(objresult.archivos)
            Dim fs As New IO.FileStream("comprobante_pago.zip", IO.FileMode.Create, IO.FileAccess.Write)
            fs.Write(vec, 0, vec.Length)
            fs.Close()
            Dim strDatosTimbre As String = ""
            strDatosTimbre &= "uuid:" & objresult.uuid & vbCrLf
            strDatosTimbre &= "fecha:" & objresult.fechadocumento & vbCrLf
            strDatosTimbre &= "fechatimbre:" & objresult.fechatimbre & vbCrLf
            strDatosTimbre &= "certificado emisor:" & objresult.seriecertificado & vbCrLf
            strDatosTimbre &= "certificado sat:" & objresult.seriecertificadosat & vbCrLf
            strDatosTimbre &= "sello sat:" & objresult.sellotimbre & vbCrLf
            strDatosTimbre &= "cadena timbre:" & objresult.cadenaoriginal & vbCrLf
            strDatosTimbre &= "sello digital:" & objresult.sellodocumento & vbCrLf
            MessageBox.Show("Comprobante generado correctamente" & vbCrLf & strDatosTimbre)
        Else
            MessageBox.Show(objresult.resultado)
        End If
    End Sub

    Private Sub BtnTimbrarNomina_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnTimbrarNomina.Click
        Dim Recibo As New ReciboNomina
        '***********************************************************************
        ' ** Informacion del recibo
        '***********************************************************************
        Recibo.serie = "N"
        Recibo.folio = 1
        Recibo.periodicidadpago = "04"
        Recibo.diaspagados = 14
        Recibo.fecha = Now.ToString("yyyy-MM-dd HH:mm:ss") 'Fecha del comprobante, no debe de exeder de 72 hrs para poder ser timbrado(ejemplo del formato 2021-04-14 12:51:03)
        Recibo.fechainicial = "2021-03-01"
        Recibo.fechafinal = "2021-03-14"
        Recibo.fechapago = "2021-02-26"
        Recibo.tiponomina = "O"
        '***********************************************************************
        ' ** Informacion del empleado
        '***********************************************************************
        Dim Empleado As New empleado
        Empleado.numero = "0001" 'codigo interno(numero de empeado)
        Empleado.nombre = "Julio Cesar Alvarado Rios"
        Empleado.rfc = "AARJ830930QL9"
        Empleado.curp = "AARJ830930HSLLSL08"
        Empleado.nss = "23058327059"
        Empleado.registropatronal = "E4949593102"
        Empleado.fechacontratacion = "2016-02-16"
        Empleado.antiguedad = "P264W" 'antiguedad en semanas del empleado 
        Empleado.tipocontrato = "01"
        Empleado.tipojornada = "01"
        Empleado.tiporegimen = "02" 'Sueldos y salarios del catalogo del SAT
        Empleado.departamento = "SISTEMAS"
        Empleado.puesto = "Analista"
        Empleado.claseriesgo = "1"
        Empleado.salariobase = 80
        Empleado.sdi = 83.62
        Empleado.entidadfederativa = "SIN"
        Empleado.banco = "002" 'BANAMEX
        'Empleado.clabe = "123456789012345678" 'clabe interbancaria
        'Agregamos la información del empleado al recibo
        Recibo.empleado = Empleado

        '***********************************************************************
        '** percepciones
        '***********************************************************************
        Dim Percepciones(2) As percepcion 'COLECCION DE PERCEPCIONES (POR EMPLEADO)
        Dim percepcion As New percepcion
        'agregamos la primer percepcion (SUELDO)
        percepcion.clave = "SDO" 'Codigo interno de la percepción
        percepcion.concepto = "SUELDO" 'Descripcion del concepto de percepción
        percepcion.importeexento = 1500 'Importe de la percepción que no graba impuestos
        percepcion.importegravado = 400 'Importe de la percepción que graba impuestos
        percepcion.tipo = "001" 'Codigo SAT de la percepción
        Percepciones(0) = percepcion
        'agregamos la 2da percepcion (puntialidad)
        percepcion = New percepcion
        percepcion.clave = "PPP"
        percepcion.concepto = "PREMIO POR PUNTUALIDAD"
        percepcion.importeexento = 200
        percepcion.importegravado = 50
        percepcion.tipo = "010"
        Percepciones(1) = percepcion

        'En caso de tener percepción por horas extras es necesario desglozarlas en simples,dobles y triples
        Dim horasextras(2) As horasextras
        'Horas extras simples
        Dim horasextra As New horasextras
        horasextra.dias = 1 'Numero de dias que trabajo horas extras
        horasextra.tipo = "01" 'Tipo de hora extra(01=Simples,02=Dobles,03=Triples)
        horasextra.horas = 3 'Cantidad de horas extras
        horasextra.importe = 60 'importe a percibir por las horas extras
        horasextras(0) = horasextra
        'Horas extras dobles
        horasextra = New horasextras
        horasextra.dias = 2
        horasextra.tipo = "02"
        horasextra.horas = 2
        horasextra.importe = 80
        horasextras(1) = horasextra
        'Horas extras triples
        horasextra = New horasextras
        horasextra.dias = 1
        horasextra.tipo = "03"
        horasextra.horas = 1
        horasextra.importe = 60
        horasextras(2) = horasextra
        'agregamos la 3ra percepcion (horas extras)
        percepcion = New percepcion
        percepcion.clave = "T EXT"
        percepcion.concepto = "TIEMPO EXTRA"
        percepcion.importeexento = 50
        percepcion.importegravado = 150
        percepcion.tipo = "019"
        percepcion.horasextras = horasextras
        Percepciones(2) = percepcion

        'Agregamos las percepciones al recibo
        Recibo.percepciones = Percepciones

        '***********************************************************************
        '** Deducciones
        '***********************************************************************
        Dim Deducciones(1) As deduccion 'COLECCION DE DEDUCCIONES (POR EMPLEADO)
        Dim deduccion As New deduccion

        'agregamos la primer deduccion (ISR)
        deduccion.clave = "ISPT" 'Codigo interno de la deducción
        deduccion.concepto = "I.S.P.T." 'Descripcion del concepto de la deducción
        deduccion.importe = 250 'Importe de la deducción
        deduccion.tipo = "002" 'Codigo SAT de la deducción
        Deducciones(0) = deduccion
        deduccion = New deduccion
        'agregamos la 2da deduccion (IMSS)
        deduccion.clave = "IMSS"
        deduccion.concepto = "Seguridad Social"
        deduccion.importe = 100
        deduccion.tipo = "001" 'codigo del catalogo del sat
        Deducciones(1) = deduccion
        'Agregamos las deducciones al recibo
        Recibo.deducciones = Deducciones


        '***********************************************************************
        '** Otros Pagos
        '***********************************************************************
        'Otros Pagos (el subsidio al empledo debe ir en otros pagos aun cuando sea cero el importe a recibir por este concepto)
        Dim OtrosPagos(0) As otropago
        Dim otroPago As New otropago
        otroPago.clave = "SEMPL" 'Codigo interno de la percepcion otro pago
        otroPago.concepto = "SUBSIDIO AL EMPLEO" 'Descripcion de la percepcion otro pago
        otroPago.importe = 0 'Importe de la percepcion otro pago
        otroPago.tipo = "002" 'Codigo SAT de la percepcion otro pago
        otroPago.subsidiocausado = 250 'importe del subsidio causado (este campo se envía solo en caso de ser el subsidio para el empleo codigo SAT 002)
        OtrosPagos(0) = otroPago
        'Agregamos las deducciones al recibo
        Recibo.otrospagos = OtrosPagos

        'Guardamos el recibo en la coleccion de recibos a timbrar
        Dim objRecibos(0) As ReciboNomina
        objRecibos(0) = Recibo
        'Mandamos generar el comprobante
        Dim Obj As New wscfdi
        Dim objresult(0) As RespuestaTimbrado
        'objresult = Obj.GenerarRecibosNomina(5090, "AAA010101AAA", 4171, objRecibos, "usrdemo", "8b60afbf328296b33204c65cdc149952")
        objresult = Obj.GenerarRecibosNomina(1, "HECF750311V64", 1, objRecibos, "pruebas", "ee2ec3cc66427bb422894495068222a8")

        If Not objresult(0).resultado Then
            MessageBox.Show(objresult(0).mensaje)
        Else
            Dim fs As New IO.FileStream("comprobante_nomina.zip", IO.FileMode.Create, IO.FileAccess.Write)
            Dim vec() As Byte
            vec = Convert.FromBase64String(objresult(0).archivos) 'El atributo archivos es un string en base64 que contiene un archivo zip con el xml, pdf y cbb.jpg
            fs.Write(vec, 0, vec.Length)
            fs.Close()

            Dim strDatosTimbre As String = ""
            strDatosTimbre &= "uuid:" & objresult(0).uuid & vbCrLf
            strDatosTimbre &= "fecha:" & objresult(0).fechadocumento & vbCrLf
            strDatosTimbre &= "fechatimbre:" & objresult(0).fechatimbre & vbCrLf
            strDatosTimbre &= "certificado emisor:" & objresult(0).seriecertificado & vbCrLf
            strDatosTimbre &= "certificado sat:" & objresult(0).seriecertificadosat & vbCrLf
            strDatosTimbre &= "sello sat:" & objresult(0).sellotimbre & vbCrLf
            strDatosTimbre &= "cadena timbre:" & objresult(0).cadenaoriginal & vbCrLf
            strDatosTimbre &= "sello digital:" & objresult(0).sellodocumento & vbCrLf
            MessageBox.Show("Recibo generado:" & vbCrLf & strDatosTimbre)
        End If
        'Next
    End Sub
End Class
