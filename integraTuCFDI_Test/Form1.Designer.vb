﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BtnTimbrarFactura = New System.Windows.Forms.Button()
        Me.BtnCancelar = New System.Windows.Forms.Button()
        Me.BtnTimbrarPago = New System.Windows.Forms.Button()
        Me.BtnTimbrarNomina = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'BtnTimbrarFactura
        '
        Me.BtnTimbrarFactura.Location = New System.Drawing.Point(12, 12)
        Me.BtnTimbrarFactura.Name = "BtnTimbrarFactura"
        Me.BtnTimbrarFactura.Size = New System.Drawing.Size(240, 30)
        Me.BtnTimbrarFactura.TabIndex = 0
        Me.BtnTimbrarFactura.Text = "Generar comprobante Factura"
        Me.BtnTimbrarFactura.UseVisualStyleBackColor = True
        '
        'BtnCancelar
        '
        Me.BtnCancelar.Location = New System.Drawing.Point(12, 120)
        Me.BtnCancelar.Name = "BtnCancelar"
        Me.BtnCancelar.Size = New System.Drawing.Size(240, 31)
        Me.BtnCancelar.TabIndex = 1
        Me.BtnCancelar.Text = "Cancelar Comprobante"
        Me.BtnCancelar.UseVisualStyleBackColor = True
        '
        'BtnTimbrarPago
        '
        Me.BtnTimbrarPago.Location = New System.Drawing.Point(12, 48)
        Me.BtnTimbrarPago.Name = "BtnTimbrarPago"
        Me.BtnTimbrarPago.Size = New System.Drawing.Size(240, 30)
        Me.BtnTimbrarPago.TabIndex = 2
        Me.BtnTimbrarPago.Text = "Generar comprobante Pago"
        Me.BtnTimbrarPago.UseVisualStyleBackColor = True
        '
        'BtnTimbrarNomina
        '
        Me.BtnTimbrarNomina.Location = New System.Drawing.Point(12, 84)
        Me.BtnTimbrarNomina.Name = "BtnTimbrarNomina"
        Me.BtnTimbrarNomina.Size = New System.Drawing.Size(240, 30)
        Me.BtnTimbrarNomina.TabIndex = 3
        Me.BtnTimbrarNomina.Text = "Generar comprobante Nómina"
        Me.BtnTimbrarNomina.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(264, 167)
        Me.Controls.Add(Me.BtnTimbrarNomina)
        Me.Controls.Add(Me.BtnTimbrarPago)
        Me.Controls.Add(Me.BtnCancelar)
        Me.Controls.Add(Me.BtnTimbrarFactura)
        Me.Name = "Form1"
        Me.Text = "IntegraTuCFDI Test"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BtnTimbrarFactura As System.Windows.Forms.Button
    Friend WithEvents BtnCancelar As System.Windows.Forms.Button
    Friend WithEvents BtnTimbrarPago As System.Windows.Forms.Button
    Friend WithEvents BtnTimbrarNomina As System.Windows.Forms.Button

End Class
