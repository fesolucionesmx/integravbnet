# Test de ayuda para consumir servicios de integratucfdi.com en lenguaje vbnet

integracionvbnet

---------------------------------------------------------------------

### Requerimientos

- Microsoft Visual basic net 2010

### Uso

En los ejemplos se encuentra la definición de las funciones que hacen consumo de los servicios de integratucfdi.com para cfdi.

**Importante**: Los datos utilizados en los ejemplos, son datos de prueba. No deben usarse para casos reales.

## Desarrollado por

- fesoluciones
